#!/bin/bash
need_install=()
get_dependency(){
    for i in $@ ; do
        if echo "${need_install[@]}" | grep -v "$i" &>/dev/null ; then
            need_install=(${need_install[@]} $i)
            get_dependency $(get_area "depends:$i" $curdir/index.txt)
        fi
    done
}
rev_depends=()
get_rev_dependency(){
    for i in $(ls $dest/info/* | grep -v "\.remove$"); do
        name=$(get_value "name" $i)
        if echo "${rev_depends[@]}" | grep -v "$name" &>/dev/null ; then
            for dep in $(get_area "dependencies" $i) ; do
                eq "$dep" "$1" && rev_depends=(${rev_depends[@]} $name)
            done
        fi
    done
    for i in $@ ; do
        rev_depends=(${rev_depends[@]} $i)
    done
}
