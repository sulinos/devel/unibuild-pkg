get_index(){
    echo $dest/$store/$1.txt
}
update_index(){
    for i in $@ ; do
        if [ -f "$i" ] ; then
            cp "$i" $dest/tmp/index.txt
        fi
        name=$(get_value_area "Name" "info" "$dest/tmp/index.txt")
        echo $name
        mv $dest/tmp/index.txt $dest/$store/$name.txt
    done
}
find_package(){
    for i in $(ls $dest/$store/*.txt) ; do
        if cat "$i" | grep -- "----package:$1----" &>/dev/null ; then
            echo "Package: $1 found in $(get_value_area name info $i)"  >&2
            echo $i
            return
        fi
    done
}
