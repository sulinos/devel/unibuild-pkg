#!/bin/bash
get_area(){
    local name="$1"
    local data="$2"
    enabled=false
    cat "$data" | while read line ; do
        if echo "$line" | grep "^----$name----" &>/dev/null ; then
        enabled=true
        read line
        fi
        if [ "$enabled" == "true" ] ; then
            echo "$line" | grep "^----" &>/dev/null && return
            echo "$line"
        fi
    done
}
get_value(){
    local name="$1"
    local data="$2"
    cat "$data" | grep -i "^$name:" | tail -n 1 | sed "s/^.*://g" | trim
}

get_value_area(){
    local name="$1"
    local area="$2"
    local data="$3"
    get_area "$area" "$data" | grep -i "^$name:" | tail -n 1 | sed "s/^.*://g" | trim

}

get_package_info(){
    mkdir -p $dest/tmp &>/dev/null
    local pkg=$(realpath $1)
    cd $dest/tmp
    rm -rf info && mkdir info
    tar --wildcards -xf "$pkg" "info" &>/dev/null
    cd $curdir
    rm -f $dest/tmp/info/*.remove
    echo $dest/tmp/info/*
    cd $curdir
}
get_value_package(){
   cd $curdir
   local file=$(get_package_info $2)
   echo $(get_value $1 $file)

}
get_area_package(){
   cd $curdir
   local file=$(get_package_info $2)
   echo $(get_area $1 $file)

}
extract(){
   cd $curdir
   local pkg=$(realpath "$1")
   cd $dest
   tar -xf $pkg
}
