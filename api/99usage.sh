usage(){
  echo "Usage: "
  echo "index            : create package index"
  echo "info             : get package info from index"
  echo "install          : install package from repo"
  echo "install-pkg      : install a package file"
  echo "list-depends     : list dependency"
  echo "list-rev-depends : list reverse dependency"
  echo "update           : update depo index"
}
