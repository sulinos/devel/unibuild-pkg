#!/bin/bash
create_index(){
    path="$1"
    name="$2"
    curpath="$(pwd)"
    echo "----info----"
    echo "Name: $name"
    echo "Build date: $(LANG=C date)"
    echo "Content: $(find $path -type f | grep ".tar$" | wc -l)"
    echo "Build host: $(hostname)"
    find $path -type f | grep ".tar$" | while read package ; do
        info_file=$(get_package_info $package)
        echo "Indexing: $package" >&2
        name=$(get_value name ${info_file})
        echo "----package:$name----"
        get_area info ${info_file}
        echo "Path:" "$package"
        echo "Hash:" $(md5sum "$curpath/$package" | cut -f 1 -d ' ')
        name=$(get_value name ${info_file})
        echo "----depends:$name----"
        get_area depends ${info_file}
        rm -rf info
    done
    echo "----end----"
    cd "$curpath"
}
