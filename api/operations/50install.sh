#!/bin/bash
fetch(){
    cd $curdir
    get_area "package:$1" "$(find_package $1)" > $dest/$store/$1
    local file=$(get_value "path" $dest/$store/$1)
    local name=$(get_value "name" $dest/$store/$1)
    if [ -f "$curdir/$file" ]; then
        if [ ! -f $dest/$store/$name.tar ] ; then
            cp "$curdir/$file" $dest/$store/$name.tar
        fi
    else
        echo "File not found: $file"
        exit 1
    fi
    
}

install_common(){
    name=$(get_value "name" $dest/$store/$1)
    echo "Installing: $name"
    if [ ! -e "$dest/info/$name" ] ; then
        extract $dest/$store/$name.tar
    else
        echo "Package $name already installed."
    fi
}
get_install(){
    cd $curdir
    get_dependency $@
    packages=${need_install[@]}
    for pkg in ${packages[@]} ; do
        fetch "$pkg"
    done
    for pkg in ${packages[@]} ; do
        install_common $pkg
    done

}
get_install_pkg(){
    for pkg in $@ ; do
        cd $curdir
        get_install $(get_area_package "depends" ${pkg})
    done
    exit 1
    for pkg in $@ ; do
        local name=$(get_value_package "name" $pkg)
        cp "$pkg" $dest/$store/$name.tar
        install_common $dest/$store/$name.tar
    done
}
