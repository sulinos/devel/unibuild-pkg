#!/bin/bash
set -eu
declare -r curdir=$(pwd)
declare -r dest="$curdir/dest"
declare -r store="pkg"
declare -r moddir="./"
for file in $(find $moddir/api -type f) ; do
    source $file
done
#rm -rf dest
mkdir -p $dest/$store
mkdir -p $dest/tmp
args=${@:2}
if [ $# -eq 0 ] ; then
    usage && exit 1
fi
case $1 in
    index)
        create_index ${args} > index.txt
        ;;
    info)
        get_area $2 index.txt
        ;;
    install)
        get_install ${args}
        ;;
    install-pkg)
        get_install_pkg ${args}
        ;;
    list-depends)
        get_dependency ${args}
        echo "${need_install[@]}"
        ;;
    list-rev-depends)
        get_rev_dependency ${args}
        echo "${rev_depends[@]}"
        ;;
    update)
        update_index ${args}
        ;;
    *)
        usage
        ;;
esac
